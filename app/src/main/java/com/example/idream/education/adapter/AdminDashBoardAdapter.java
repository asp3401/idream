package com.example.idream.education.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.idream.R;
import com.example.idream.databinding.ClassItemBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AdminDashBoardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<String> classList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    AdminDashBoardAdapter.AdminDashBoardInteraction interaction;
    // private NotesInteractionListener listener;

    public void setData(List<String> classList, boolean clearPreviousData) {
        if (clearPreviousData) {
            this.classList.clear();
        }
        this.classList.addAll(classList);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ClassItemBinding binding;

        public MyViewHolder(final ClassItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdminDashBoardAdapter(AdminDashBoardAdapter.AdminDashBoardInteraction interaction) {
        this.interaction=interaction;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        return new AdminDashBoardAdapter.MyViewHolder(((ClassItemBinding) DataBindingUtil.inflate(layoutInflater, R.layout.class_item, parent, false)));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ((AdminDashBoardAdapter.MyViewHolder) holder).binding.setClassName(classList.get(position));
        ((AdminDashBoardAdapter.MyViewHolder) holder).binding.setInteraction(interaction);

    }

    @Override
    public int getItemCount() {

        return classList == null ? 0 : classList.size();

    }

    public interface AdminDashBoardInteraction {
        public void onClassClicked(String className);
    }
}
