package com.example.idream.education.viewmodel.viewmodelfactory;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.idream.education.viewmodel.AddVieoActViewModel;
import com.example.idream.education.viewmodel.ClassVideosActViewModel;

public class ClassVideoViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private String mParam;


    public ClassVideoViewModelFactory(Application application, String param) {
        mApplication = application;
        mParam = param;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new ClassVideosActViewModel(mApplication, mParam);
    }
}
