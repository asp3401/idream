package com.example.idream.education.view;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.idream.R;
import com.example.idream.databinding.ActivityDashBoardBinding;
import com.example.idream.education.preference.PrefManager;
import com.example.idream.education.utils.CommonMethods;
import com.google.android.material.navigation.NavigationView;

public class DashBoardActivity extends AppCompatActivity {
    ActivityDashBoardBinding activityDashBoardBinding;
    Fragment currFrag;
    UserDashboardFragment userDashboardFragment;
    AdminDashBoardFragment adminDashBoardFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDashBoardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dash_board);
        userDashboardFragment = new UserDashboardFragment();
        adminDashBoardFragment = new AdminDashBoardFragment();
        setUpNavigationView();
        addFragment(PrefManager.isAdminLoggedIn(getApplicationContext()) ? adminDashBoardFragment:userDashboardFragment);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        activityDashBoardBinding.navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        if (PrefManager.isAdminLoggedIn(getApplicationContext()))
                            addFragment(adminDashBoardFragment);
                        else addFragment(userDashboardFragment);
                        break;
                    case R.id.nav_logout:
                        CommonMethods.logoutOrSessionExp(getApplicationContext());
                        break;
                    default:
                        if (PrefManager.isAdminLoggedIn(getApplicationContext()))
                            addFragment(adminDashBoardFragment);
                        else addFragment(userDashboardFragment);
                }

          /*      //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();*/
                closeDrawer();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, activityDashBoardBinding.drawerLayout, activityDashBoardBinding.appBarMain.toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        activityDashBoardBinding.drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up

        activityDashBoardBinding.appBarMain.toolbar.setNavigationIcon(R.drawable.ic_menu);
    }

    public void addFragment(Fragment fragment) {
        currFrag = fragment;

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, currFrag).commit();


    }

    private boolean closeDrawer() {
        try {
            activityDashBoardBinding.drawerLayout.closeDrawers();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
