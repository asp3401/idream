package com.example.idream.education.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.idream.R;
import com.example.idream.databinding.ClassItemBinding;
import com.example.idream.databinding.VideoItemBinding;
import com.example.idream.education.model.Video;

import java.util.ArrayList;
import java.util.List;

public class ClassVideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Video> videoList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    ClassVideosAdapter.VideosInteraction interaction;
    // private NotesInteractionListener listener;

    public void setData(List<Video> videoList, boolean clearPreviousData) {
        if (clearPreviousData) {
            this.videoList.clear();
        }
        this.videoList.addAll(videoList);
        notifyDataSetChanged();
    }

    public void addVideo(Video video) {

        this.videoList.add(video);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final VideoItemBinding videoItemBinding;

        public MyViewHolder(final VideoItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.videoItemBinding = itemBinding;
        }
    }


    public ClassVideosAdapter(ClassVideosAdapter.VideosInteraction interaction) {
        this.interaction = interaction;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        return new ClassVideosAdapter.MyViewHolder(((VideoItemBinding) DataBindingUtil.inflate(layoutInflater, R.layout.video_item, parent, false)));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ((ClassVideosAdapter.MyViewHolder) holder).videoItemBinding.setVideo(videoList.get(position));
        ((ClassVideosAdapter.MyViewHolder) holder).videoItemBinding.setInteraction(interaction);

    }

    @Override
    public int getItemCount() {

        return videoList == null ? 0 : videoList.size();

    }

    public interface VideosInteraction {
        public void onVideoClicled(Video video);
    }
}
