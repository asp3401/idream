package com.example.idream.education.model.responsemodel;

public class LoginResponseModel {
    boolean isLoggedIn;
    String message="";

    public LoginResponseModel(boolean isLoggedIn, String message) {
        this.isLoggedIn = isLoggedIn;
        this.message = message;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public String getMessage() {
        return message;
    }
}
