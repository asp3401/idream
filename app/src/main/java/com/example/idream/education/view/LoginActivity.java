package com.example.idream.education.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.idream.R;
import com.example.idream.databinding.ActivityLoginBinding;
import com.example.idream.education.model.User;
import com.example.idream.education.model.responsemodel.LoginResponseModel;
import com.example.idream.education.model.responsemodel.RegisterResponseModel;
import com.example.idream.education.preference.PrefManager;
import com.example.idream.education.viewmodel.LoginActViewModel;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding activityLoginBinding;
    LoginActViewModel loginActViewModel;
    User user;
    MutableLiveData<Boolean> mutableLiveDataProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginActViewModel = ViewModelProviders.of(this).get(LoginActViewModel.class);
        user = new User();
        activityLoginBinding.setUser(user);
        loginActViewModel.getMutableLiveDataProgress().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                activityLoginBinding.setShowProgress(aBoolean);
            }
        });


    }

    public void login(View view) {
        if (isValidate()) {
            loginActViewModel.login(user).observe(this, new Observer<LoginResponseModel>() {
                @Override
                public void onChanged(LoginResponseModel loginResponseModel) {
                    if (loginResponseModel==null)
                        return;

                    if (loginResponseModel.isLoggedIn()){
                        Toast.makeText(LoginActivity.this, loginResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                        loginActViewModel.saveData(user);
                        goToNextActivity();
                    }
                    else {
                        Toast.makeText(LoginActivity.this, loginResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    public boolean isValidate() {
        if (TextUtils.isEmpty(user.getEmail())) {
            Toast.makeText(this, R.string.invalid_email, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(user.getPassword())) {
            Toast.makeText(this, R.string.password_cant_be_empty, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    void goToNextActivity(){
        Intent intent=null;
        if (PrefManager.isAdminLoggedIn(getApplicationContext())){
            intent = new Intent(LoginActivity.this, DashBoardActivity.class);
        }else {
            intent = new Intent(LoginActivity.this, SelectClassActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
