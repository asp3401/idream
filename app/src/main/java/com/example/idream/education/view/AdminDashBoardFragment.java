package com.example.idream.education.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.idream.R;
import com.example.idream.databinding.FragmentAdminDashBoardBinding;
import com.example.idream.education.adapter.AdminDashBoardAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminDashBoardFragment extends Fragment implements AdminDashBoardAdapter.AdminDashBoardInteraction {
    LinearLayoutManager linearLayoutManager;
    AdminDashBoardAdapter adminDashBoardAdapter;

    public AdminDashBoardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  return inflater.inflate(R.layout.fragment_admin_dash_board, container, false);
        FragmentAdminDashBoardBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_admin_dash_board, container, false);
       // linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recylerview.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adminDashBoardAdapter = new AdminDashBoardAdapter(this);
        binding.recylerview.setAdapter(adminDashBoardAdapter);
        List<String> list = Arrays.asList(getResources().getStringArray(R.array.classes_array));
        adminDashBoardAdapter.setData(list,false);
      //  Toast.makeText(getContext(), list.size()+"", Toast.LENGTH_SHORT).show();

        return binding.getRoot();
    }

    @Override
    public void onClassClicked(String className) {
     //   Toast.makeText(getContext(), className, Toast.LENGTH_SHORT).show();
        startActivity(ClassVideosActivity.setIntent(getContext(),className));

    }
}
