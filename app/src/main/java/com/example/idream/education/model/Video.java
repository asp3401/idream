package com.example.idream.education.model;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.idream.R;

public class Video {

    String thumbnail = "";
    String url = "";

    public Video(String thumbnail, String url) {

        this.thumbnail = thumbnail;
        this.url = url;
    }
    public Video(){}


    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void imageUrl(final View view, String imageUrl) {
        Log.d("TRACKER","iwas here");
        Glide.with(view.getContext())
                .load(imageUrl)
               // .apply(new RequestOptions().circleCrop())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        view.findViewById(R.id.flagProgressBar).setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        view.findViewById(R.id.flagProgressBar).setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(((ImageView) view.findViewById(R.id.flagImageview)));
        Log.d("TRACKER","iwas here");
    }
}
