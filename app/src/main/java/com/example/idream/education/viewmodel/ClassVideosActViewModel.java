package com.example.idream.education.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.idream.education.model.Video;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ClassVideosActViewModel extends AndroidViewModel {
    DatabaseReference rootRef;
    DatabaseReference classRef;

    public ClassVideosActViewModel(@NonNull Application application, String className) {
        super(application);
        rootRef = FirebaseDatabase.getInstance().getReference();
        classRef = rootRef.child(className);
    }

    public LiveData<Video> getVideo() {
        final MutableLiveData<Video> mutableLiveData = new MutableLiveData<>();
        classRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Video video = dataSnapshot.getValue(Video.class);
                mutableLiveData.setValue(video);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return mutableLiveData;


    }
}
