package com.example.idream.education.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.idream.R;
import com.example.idream.databinding.ActivityClassVideosBinding;
import com.example.idream.education.adapter.ClassVideosAdapter;
import com.example.idream.education.model.Video;
import com.example.idream.education.viewmodel.AddVieoActViewModel;
import com.example.idream.education.viewmodel.ClassVideosActViewModel;
import com.example.idream.education.viewmodel.viewmodelfactory.ClassVideoViewModelFactory;
import com.example.idream.education.viewmodel.viewmodelfactory.MyViewModelFactory;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ClassVideosActivity extends AppCompatActivity implements ClassVideosAdapter.VideosInteraction {
    ActivityClassVideosBinding binding;
    public static final int pREQUEST_CODE_TO_UPLOAD_VIDEO = 54;
    ClassVideosAdapter adapter;
    ClassVideosActViewModel viewModel;
    String className = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.activity_class_videos);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_class_videos);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.class_videos);
        binding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
        setArgs();
        viewModel = ViewModelProviders.of(this, new ClassVideoViewModelFactory(this.getApplication(), className)).get(ClassVideosActViewModel.class);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        binding.recylerview.setLayoutManager(mLayoutManager);
        adapter = new ClassVideosAdapter(this);
        binding.recylerview.setAdapter(adapter);
        viewModel.getVideo().observe(this, new Observer<Video>() {
            @Override
            public void onChanged(Video video) {
                adapter.addVideo(video);
                // we will add video one by one from firebase
            }
        });


    }

    public static Intent setIntent(Context context, String className) {
        Intent intent = new Intent(context, ClassVideosActivity.class);
        intent.putExtra("className", className);
        return intent;
    }

    public void goToNextActivity(View view) {
        startActivityForResult(AddVideoActivity.setIntent(ClassVideosActivity.this, getIntent().getStringExtra("className")), pREQUEST_CODE_TO_UPLOAD_VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case pREQUEST_CODE_TO_UPLOAD_VIDEO:
                    String thumbnail = data.getStringExtra("thumbnail");
                    String url = data.getStringExtra("url");
                    Log.d("ASPPPP", url);

            //        adapter.addVideo(new Video(thumbnail, url));
                    break;
            }
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onVideoClicled(Video video) {
        startActivity(WebViewActivity.setIntent(ClassVideosActivity.this, video.getUrl()));

    }

    void setArgs() {
        Intent intent = getIntent();
        if (intent != null) {
            this.className = getIntent().getStringExtra("className");
        }
    }
}
