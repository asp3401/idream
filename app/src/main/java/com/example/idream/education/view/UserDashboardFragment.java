package com.example.idream.education.view;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.idream.R;
import com.example.idream.databinding.FragmentAdminDashBoardBinding;
import com.example.idream.databinding.FragmentUserDashboardBinding;
import com.example.idream.education.adapter.ClassVideosAdapter;
import com.example.idream.education.model.Video;
import com.example.idream.education.viewmodel.UserDashboardFragViewModel;


public class UserDashboardFragment extends Fragment implements ClassVideosAdapter.VideosInteraction {

    ClassVideosAdapter adapter;
    UserDashboardFragViewModel viewModel;

    public UserDashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentUserDashboardBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_user_dashboard, container, false);
        viewModel = ViewModelProviders.of(this).get(UserDashboardFragViewModel.class);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        binding.recylerview.setLayoutManager(mLayoutManager);
        adapter = new ClassVideosAdapter(this);
        binding.recylerview.setAdapter(adapter);
        viewModel.getVideo().observe(getViewLifecycleOwner(), new Observer<Video>() {
            @Override
            public void onChanged(Video video) {
                adapter.addVideo(video);
                // we will add video one by one from firebase
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onVideoClicled(Video video) {
        startActivity(WebViewActivity.setIntent(getActivity(), video.getUrl()));
    }
}
