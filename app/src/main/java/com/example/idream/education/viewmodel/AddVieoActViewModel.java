package com.example.idream.education.viewmodel;

import android.app.Application;
import android.content.ClipData;
import android.content.ClipboardManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.idream.education.model.Video;
import com.example.idream.education.utils.CommonMethods;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.Context.CLIPBOARD_SERVICE;

public class AddVieoActViewModel extends AndroidViewModel {
    ClipboardManager clipboardManager;
    DatabaseReference databaseReference;

    public AddVieoActViewModel(@NonNull Application application,String className) {
        super(application);
        Object clipboardService = application.getSystemService(CLIPBOARD_SERVICE);
        clipboardManager = (ClipboardManager) clipboardService;
        databaseReference = FirebaseDatabase.getInstance().getReference().child(className);
    }

    public String getLastCopiedItem() {
        String url = "";
        try {
            ClipData clipData = clipboardManager.getPrimaryClip();
            // Get item count.
            int itemCount = clipData.getItemCount();
            if (itemCount > 0) {
                // Get source text.
                ClipData.Item item = clipData.getItemAt(0);
                url = item.getText().toString();

            }
        } catch (Exception e) {
        }
        return url;
    }
    public void upload(Video video){
        databaseReference.child(CommonMethods.random()).setValue(video);
    }

}
