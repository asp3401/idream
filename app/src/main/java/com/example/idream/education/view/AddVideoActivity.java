package com.example.idream.education.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.idream.R;
import com.example.idream.databinding.ActivityAddVideoBinding;
import com.example.idream.education.model.Video;
import com.example.idream.education.viewmodel.AddVieoActViewModel;
import com.example.idream.education.viewmodel.viewmodelfactory.MyViewModelFactory;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.idream.education.utils.CommonMethods.getYoutubeThumbnailUrlFromVideoUrl;

public class AddVideoActivity extends AppCompatActivity {
    ActivityAddVideoBinding activityAddVideoBinding;
    AddVieoActViewModel addVieoActViewModel;
    Video video;
    String className = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityAddVideoBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_video);
        setArguements();
        // addVieoActViewModel=new AddVieoActViewModelFa(this.getApplication(),"1");
        //addVieoActViewModel = ViewModelProviders.of(this, addVieoActViewModel).get(AddVieoActViewModel.class);

        addVieoActViewModel = ViewModelProviders.of(this, new MyViewModelFactory(this.getApplication(), className)).get(AddVieoActViewModel.class);
    }

    public void pasteUrl(View view) {
        try {
            String url = addVieoActViewModel.getLastCopiedItem();
            if (isValidUrl(url)) {
                String thumnail = getYoutubeThumbnailUrlFromVideoUrl(url);
                video = new Video(thumnail, url);
                activityAddVideoBinding.setVideo(video);
                activityAddVideoBinding.setIsVideoLoaded(true);
                Toast.makeText(this, "Pasted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Invalid Url", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {

        }

    }

    public boolean isValidUrl(String url) {
        return url.contains("you");
    }

    public void Upload(View view) {
        if (video != null) {
            addVieoActViewModel.upload(video);
        }
        Intent intent = new Intent();
        intent.putExtra("thumbnail", video.getThumbnail());
        intent.putExtra("url", video.getUrl());
        setResult(RESULT_OK, intent);
        finish();

    }

    public static Intent setIntent(Context context, String className) {
        Intent intent = new Intent(context, AddVideoActivity.class);
        intent.putExtra("className", className);
        return intent;

    }

    void setArguements() {
        Intent intent = getIntent();
        if (intent != null) {
            this.className = intent.getStringExtra("className");
        }
    }
}
