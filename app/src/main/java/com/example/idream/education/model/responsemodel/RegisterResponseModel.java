package com.example.idream.education.model.responsemodel;

public class RegisterResponseModel {
    boolean isRegistered;
    String message="";

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    public RegisterResponseModel(boolean isRegistered, String message) {
        this.isRegistered = isRegistered;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }




}
