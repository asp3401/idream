package com.example.idream.education.preference;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    private static SharedPreferences mSharedPreferences;

    private static SharedPreferences.Editor mEditor;

    public static void getInstance(Context context) {
        if(context!=null)
            mSharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
    }

    public static void clearData(Context pContext) {
        getEditorInstance(pContext);
        mEditor.clear().apply();
    }

    private static void getEditorInstance(Context context) {
        if (mSharedPreferences == null)
            getInstance(context);
        mEditor = mSharedPreferences.edit();
    }

    public static void setEmail(Context context, String email) {
        if (mEditor == null) {
            getEditorInstance(context);
        }
        mEditor.putString("email", email.trim()).apply();
    }

    public static String getEmail(Context context) {
        if (mSharedPreferences == null)
            getInstance(context);
        return mSharedPreferences.getString("email", null);
    }

    public static boolean isAdminLoggedIn(Context context) {
        if (mSharedPreferences == null)
            getInstance(context);
        return mSharedPreferences.getBoolean("isAdminLoggedIn", false);
    }

    public static void setIsAdminLogin(Context context, boolean isAdminLoggedIn) {
        getEditorInstance(context);
        mEditor.putBoolean("isAdminLoggedIn", isAdminLoggedIn).apply();
    }

    public static boolean isLoggedIn(Context context) {
        if (mSharedPreferences == null)
            getInstance(context);
        return mSharedPreferences.getBoolean("isLoggedIn", false);
    }

    public static void setIsLoggedIn(Context context, boolean isLoggedIn) {
        getEditorInstance(context);
        mEditor.putBoolean("isLoggedIn", isLoggedIn).apply();
    }

    public static void setClassName(Context context, String className) {
        if (mEditor == null) {
            getEditorInstance(context);
        }
        mEditor.putString("className", className.trim()).apply();
    }

    public static String getClassName(Context context) {
        if (mSharedPreferences == null)
            getInstance(context);
        return mSharedPreferences.getString("className", null);
    }



}
