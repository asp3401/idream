package com.example.idream.education.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.idream.R;
import com.example.idream.databinding.ActivityRegisterBinding;
import com.example.idream.education.model.User;
import com.example.idream.education.model.responsemodel.RegisterResponseModel;
import com.example.idream.education.utils.CommonMethods;
import com.example.idream.education.viewmodel.RegisterActViewModel;

public class RegisterActivity extends AppCompatActivity {
    ActivityRegisterBinding activityRegisterBinding;
    User user;
    RegisterActViewModel registerActViewModel;
    LiveData<Boolean> progressLiveData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_register);
        activityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        user = new User();
        registerActViewModel = ViewModelProviders.of(this).get(RegisterActViewModel.class);
        progressLiveData = registerActViewModel.getProgress();
        progressLiveData.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                activityRegisterBinding.setShowProgress(aBoolean);
            }
        });
        activityRegisterBinding.setUser(user);
    }

    public void signUp(View view) {
        if (isValidate()){
            registerActViewModel.register(user).observe(this, new Observer<RegisterResponseModel>() {
                @Override
                public void onChanged(RegisterResponseModel registerResponseModel) {
                  if (registerResponseModel==null)
                      return;

                  if (registerResponseModel.isRegistered()){
                      Toast.makeText(RegisterActivity.this, registerResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                  }else {
                      Toast.makeText(RegisterActivity.this, registerResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                  }
                }
            });


        }


    }

    public boolean isValidate() {
        if (!CommonMethods.isValidEmail(user.getEmail())) {
            Toast.makeText(this, R.string.invalid_email, Toast.LENGTH_LONG).show();
            return false;
        }
        if (!CommonMethods.isValidPassword(user.getPassword())) {
            Toast.makeText(this, R.string.invalid_password, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void goToLoginActivity(View view){
        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
    }
}
