package com.example.idream.education.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.idream.R;
import com.example.idream.databinding.ActivityWebViewBinding;

public class WebViewActivity extends AppCompatActivity {
ActivityWebViewBinding activity_web_view;
String url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity_web_view= DataBindingUtil.setContentView(this,R.layout.activity_web_view);
        setSupportActionBar(activity_web_view.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        activity_web_view.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        activity_web_view.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
        setArgs();

        activity_web_view.webview.getSettings().setJavaScriptEnabled(true);
        clear();
        activity_web_view.webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
             //   showToast(getApplicationContext(),getLayoutInflater(),description);
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
        activity_web_view.webview.loadUrl(url);
    }
    public static Intent setIntent(Context context , String url){
        Intent intent=new Intent(context,WebViewActivity.class);
        intent.putExtra("url",url);
        return intent;
    }
    void setArgs(){
        Intent intent=getIntent();
        if (intent!=null){
            this.url=intent.getStringExtra("url");
        }
    }
    public void clear(){
        activity_web_view.webview.clearHistory();
        activity_web_view.webview.clearFormData();
        activity_web_view.webview.clearCache(true);
        activity_web_view.webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    }
}
