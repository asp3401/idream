package com.example.idream.education.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.idream.R;
import com.example.idream.education.model.User;
import com.example.idream.education.model.responsemodel.LoginResponseModel;
import com.example.idream.education.preference.PrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActViewModel extends AndroidViewModel {
    MutableLiveData<LoginResponseModel> loginResponseModelMutableLiveData ;
    MutableLiveData<Boolean> mutableLiveDataProgress;

    FirebaseAuth mAuth;

    public LoginActViewModel(@NonNull Application application) {
        super(application);
        mAuth = FirebaseAuth.getInstance();
        mutableLiveDataProgress = new MutableLiveData<>();
    }

    public LiveData<LoginResponseModel> login(User user) {
        loginResponseModelMutableLiveData = new MutableLiveData<>();
        mutableLiveDataProgress.setValue(true);
        mAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    loginResponseModelMutableLiveData.setValue(new LoginResponseModel(true,
                            getApplication().getString(R.string.successfully_loggedin)));

                } else {
                    loginResponseModelMutableLiveData.setValue(new LoginResponseModel(true,
                            task.getException().getMessage()));
                    //    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
                mutableLiveDataProgress.setValue(false);
            }

        });
        return loginResponseModelMutableLiveData;
    }

    public MutableLiveData<Boolean> getMutableLiveDataProgress() {
        return mutableLiveDataProgress;
    }
    public void saveData(User user){
        PrefManager.setEmail(getApplication(),user.getEmail());
        PrefManager.setIsAdminLogin(getApplication(),
                user.getEmail().equals("admin@gmail.com"));
        PrefManager.setIsLoggedIn(getApplication(),true);
    }
}
