package com.example.idream.education.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.idream.R;
import com.example.idream.education.model.User;
import com.example.idream.education.model.responsemodel.RegisterResponseModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class RegisterActViewModel extends AndroidViewModel {
    FirebaseAuth mAuth;
    MutableLiveData<Boolean> mutableLiveDataProgress;
    MutableLiveData<RegisterResponseModel> responseModel;


    public RegisterActViewModel(@NonNull Application application) {
        super(application);
        mAuth = FirebaseAuth.getInstance();
        mutableLiveDataProgress = new MutableLiveData<>();


    }

    public LiveData<RegisterResponseModel> register(User user) {
        responseModel=new MutableLiveData<>();
        mutableLiveDataProgress.setValue(true);

        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    responseModel.setValue(new RegisterResponseModel(true,
                            getApplication().getApplicationContext().getString(R.string.succesfully_registered)));


                } else {
                    Log.d("ASP", "error");
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        responseModel.setValue(new RegisterResponseModel(false,
                                getApplication().getApplicationContext().getString(R.string.alread_registered)));

                    } else {
                        //   Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        responseModel.setValue(new RegisterResponseModel(false,
                                task.getException().getMessage()));
                    }

                }
                mutableLiveDataProgress.setValue(false);

            }
        });
        return responseModel;
    }

    public LiveData<Boolean> getProgress() {
        return mutableLiveDataProgress;
    }


}
