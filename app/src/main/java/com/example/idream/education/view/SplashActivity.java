package com.example.idream.education.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.example.idream.R;
import com.example.idream.databinding.ActivitySplashBinding;
import com.example.idream.education.preference.PrefManager;

public class SplashActivity extends AppCompatActivity {

    public static int SPLASH_TIME_OUT = 2000;
    ActivitySplashBinding activitySplashBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        goToNextActivity();
    }

    void goToNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                if (PrefManager.isLoggedIn(getApplicationContext())) {
                    if (PrefManager.isAdminLoggedIn(getApplicationContext())){
                        intent = new Intent(getApplication(), DashBoardActivity.class);
                    }else {
                        boolean isClassSelected = !TextUtils.isEmpty(PrefManager.getClassName(getApplicationContext()));
                        if (isClassSelected){
                            intent = new Intent(getApplication(), DashBoardActivity.class);
                        }else {
                           intent=new Intent(getApplication(),SelectClassActivity.class);
                        }
                    }

                } else {
                    intent = new Intent(getApplication(), RegisterActivity.class);
                }

                startActivity(intent);
                finish();

            }

        }, SPLASH_TIME_OUT);
    }
}
