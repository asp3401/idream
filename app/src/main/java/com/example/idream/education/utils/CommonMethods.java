package com.example.idream.education.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.example.idream.education.view.RegisterActivity;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethods {
    public final static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public final static boolean isValidPassword(String target) {
        if (target.length() > 5) {
            return true;
        } else return false;

    }
    public static boolean logoutOrSessionExp(Context context) {
        try {
            //  DBHelper.clearDB(context);

            SharedPreferences preferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
            preferences.edit().clear().apply();
            context.startActivity(new Intent(context, RegisterActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));


        } catch (Exception e) {
            Log.d("LOGOUTRACKER", e.getLocalizedMessage());
            return false;
        }
        return true;

    }
    public static String getYoutubeThumbnailUrlFromVideoUrl(String videoUrl) {
        return "http://img.youtube.com/vi/" + exctractVideoID(videoUrl) + "/0.jpg";
    }

    public static String exctractVideoID(String inUrl) {
        inUrl = inUrl.replace("&feature=youtu.be", "");
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }
    public static String random() {
        int min = 65;
        int max = 80000;

        Random r = new Random();
        int i1 = r.nextInt(max - min + 1) + min;
        return "random_key"+i1;
    }
}
